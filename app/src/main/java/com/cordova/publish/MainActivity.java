package com.cordova.publish;

import android.os.Bundle;
import android.view.View;

import com.cordova.publish.databinding.ActivityMainBinding;
import com.ffsky.cordova.CordovaKit;
import com.ffsky.cordova.app.WebActivity;
import com.ffsky.smtt.X5ModularLoader;
import com.ffsky.smtt.X5WebActivity;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        //
        binding.btnCordovaActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebActivity.start(MainActivity.this, null);
            }
        });

        binding.btnCordovaFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CordovaFragmentActivity.start(MainActivity.this);
            }
        });
        binding.btnWebActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebActivity.start(MainActivity.this, null);
            }
        });

        binding.btnUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebActivity.start(MainActivity.this, "https://gitee.com/hljdrl/cordova-publish");
            }
        });
        binding.btnHomeTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeTabActivity.start(MainActivity.this);
            }
        });

        binding.btnX5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                X5WebActivity.start(MainActivity.this, "http://soft.imtt.qq.com/browser/tes/feedback.html");
                X5WebActivity.start(MainActivity.this, "http://debugtbs.qq.com");
            }
        });
        binding.btnX5webview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                X5WebActivity.start(MainActivity.this,null);
            }
        });


        CordovaKit.getInstance().load(getApplicationContext());

        /**
         * 实际代码接入，最好放到 Application的onCreate方法中初始化，提前加载下载X5内核
         *
         * */
        X5ModularLoader.getInstance().load(getApplication(), null);
    }

}