package com.cordova.publish;

import android.app.Activity;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;

import com.cordova.publish.databinding.ActivityTablayoutV1Binding;
import com.ffsky.cordova.adapter.FFSkyPageAdapter;
import com.ffsky.cordova.app.BaseTabWebActivity;
import com.ffsky.cordova.fragment.AdobeCordovaFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

public class HomeTabActivity extends BaseTabWebActivity {


    public static void start(Activity activity) {
        Intent i = new Intent(activity, HomeTabActivity.class);
        activity.startActivity(i);
    }

    private ActivityTablayoutV1Binding binding;
    private FFSkyPageAdapter pageAdapter;
    @Override
    protected View buildRootView() {
        if (binding == null) {
            binding = ActivityTablayoutV1Binding.inflate(getLayoutInflater());
        }
        return binding.getRoot();
    }


    @Override
    protected BottomNavigationView buildBottomNavigationView() {
        return binding.tabBar;
    }

    @Override
    protected boolean isUserInputEnabled() {
        return false;
    }

    @Override
    protected int onOffscreenPageLimit() {
        if (pageAdapter != null) {
            return pageAdapter.getItemCount();
        }
        return super.onOffscreenPageLimit();
    }

    @Override
    protected FragmentStateAdapter buildFragmentStateAdapter() {
        if (pageAdapter == null) {
            List<Fragment> list = new ArrayList();
            list.add(new AdobeCordovaFragment());
            list.add(new AdobeCordovaFragment());
            list.add(new AdobeCordovaFragment());
            list.add(new AdobeCordovaFragment());
            pageAdapter = new FFSkyPageAdapter(this, list);
        }
        return pageAdapter;
    }

    @Override
    protected ViewPager2 buildViewPager2() {
        return binding.viewPager;
    }

    @Override
    protected int onMenuItemOfIndex(MenuItem item) {
        final int menuId = item.getItemId();
        if (menuId == R.id.item_news) {
            return 0;
        } else if (menuId == R.id.item_lib) {
            return 1;
        } else if (menuId == R.id.item_find) {
            return 2;
        } else if (menuId == R.id.item_more) {
            return 3;
        }
        return 0;
    }


}
