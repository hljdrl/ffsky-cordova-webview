package com.ffsky.cordova.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.ffsky.cordova.CordovaKit;

public class WebActivity extends AppCompatWebActivity {


    public static void start(Activity activity, String url) {
        Intent i = new Intent(activity, WebActivity.class);
        if (!TextUtils.isEmpty(url)) {
            i.putExtra(CordovaKit.KEY_URL, url);
        }
        activity.startActivity(i);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        if (i.hasExtra(CordovaKit.KEY_URL)) {
            launchUrl = i.getStringExtra(CordovaKit.KEY_URL);
        }
        loadUrl(launchUrl);
    }
}
