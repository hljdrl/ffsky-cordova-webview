package com.ffsky.cordova.app;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

public abstract class BaseTabWebActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {


    private static class OnPageChangeCallbackImpl extends ViewPager2.OnPageChangeCallback {

        private BottomNavigationView bottomNavigationView;

        public OnPageChangeCallbackImpl(BottomNavigationView view) {
            bottomNavigationView = view;
        }

        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            if (bottomNavigationView != null) {
                int size = bottomNavigationView.getMenu().size();
                if (position <= size) {
                    bottomNavigationView.getMenu().getItem(position).setChecked(true);
                }
            }
        }

        public void onDestroy() {
            bottomNavigationView = null;
        }

    }

    private ViewPager2 mViewPager2;
    private FragmentStateAdapter fragmentStateAdapter;
    private BottomNavigationView mBottomNavigationView;
    private OnPageChangeCallbackImpl mOnPageChangeCallbackImpl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = buildRootView();
        setContentView(rootView);
        mViewPager2 = buildViewPager2();
        mBottomNavigationView = buildBottomNavigationView();
        fragmentStateAdapter = buildFragmentStateAdapter();
        mOnPageChangeCallbackImpl = new OnPageChangeCallbackImpl(mBottomNavigationView);
        if (mViewPager2 != null && fragmentStateAdapter != null) {
            mViewPager2.setOffscreenPageLimit(onOffscreenPageLimit());
            mViewPager2.setUserInputEnabled(isUserInputEnabled());

            mViewPager2.setAdapter(fragmentStateAdapter);
            mViewPager2.registerOnPageChangeCallback(mOnPageChangeCallbackImpl);
        }
        if (mBottomNavigationView != null) {
            mBottomNavigationView.setOnNavigationItemSelectedListener(this);
        }
    }

    protected boolean isUserInputEnabled(){
        return true;
    }

    protected int onOffscreenPageLimit() {
        return ViewPager2.OFFSCREEN_PAGE_LIMIT_DEFAULT;
    }

    @Override
    protected void onDestroy() {
        if (mBottomNavigationView != null) {
            mBottomNavigationView.setOnNavigationItemSelectedListener(null);
        }
        if (mViewPager2 != null && mOnPageChangeCallbackImpl != null) {
            mViewPager2.unregisterOnPageChangeCallback(mOnPageChangeCallbackImpl);
            //
            mOnPageChangeCallbackImpl.onDestroy();
            mOnPageChangeCallbackImpl = null;
        }
        super.onDestroy();

    }

    protected abstract View buildRootView();

    protected abstract BottomNavigationView buildBottomNavigationView();

    protected abstract FragmentStateAdapter buildFragmentStateAdapter();

    protected abstract ViewPager2 buildViewPager2();

    protected abstract int onMenuItemOfIndex(MenuItem item);

    protected void jitSelectViewPager2(int select) {
        if (mViewPager2 != null && select > -1) {
            mViewPager2.setCurrentItem(select, false);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        final int index = onMenuItemOfIndex(item);
        jitSelectViewPager2(index);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


}
