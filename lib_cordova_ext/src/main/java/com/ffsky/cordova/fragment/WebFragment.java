package com.ffsky.cordova.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;


import com.ffsky.cordova.R;

import org.apache.cordova.LOG;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class WebFragment extends CordovaFragment {

    private ViewGroup mViewGroup;

    @Override
    public void onCreate(@Nullable  Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull  LayoutInflater inflater, @Nullable  ViewGroup container, @Nullable  Bundle savedInstanceState) {
        LOG.d(TAG, "WebFragment.onCreateView()");
        View view = inflater.inflate(R.layout.lib_cordova_ext_web_fragment, container, false);
        mViewGroup = view.findViewById(R.id.lib_hybrid_web_layout);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull  View view, @Nullable  Bundle savedInstanceState) {
        LOG.d(TAG, "WebFragment.onViewCreated()");
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected void onWebViewCreate(View webView) {
        LOG.d(TAG, "WebFragment.onWebViewCreate()");
        mViewGroup.addView(webView);
    }

    @Override
    public ViewGroup.LayoutParams buildWebViewLayoutParams() {
        return new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onActivityCreated(@Nullable  Bundle savedInstanceState) {
        LOG.d(TAG, "WebFragment.onActivityCreated()");
        super.onActivityCreated(savedInstanceState);
        loadUrl(mParser.getLaunchUrl());
    }
}
