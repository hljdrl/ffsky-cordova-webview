package com.ffsky.cordova.adapter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class FFSkyPageAdapter extends FragmentStateAdapter {

    private List<Fragment> list = new ArrayList<Fragment>();
    public FFSkyPageAdapter(@NonNull FragmentActivity fragmentActivity, List<Fragment> fts) {
        super(fragmentActivity);
        if (fts != null) {
            list.addAll(fts);
        }
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if (position < list.size()) {
            return list.get(position);
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
