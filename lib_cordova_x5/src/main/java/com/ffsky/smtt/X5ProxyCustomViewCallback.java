package com.ffsky.smtt;

import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient;

public class X5ProxyCustomViewCallback implements android.webkit.WebChromeClient.CustomViewCallback {


    private IX5WebChromeClient.CustomViewCallback callback;

    public X5ProxyCustomViewCallback(IX5WebChromeClient.CustomViewCallback customViewCallback) {
        callback = customViewCallback;
    }

    @Override
    public void onCustomViewHidden() {
        if (callback != null) {
            callback.onCustomViewHidden();
        }
    }
}
