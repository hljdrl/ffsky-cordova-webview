package com.ffsky.smtt;

public class X5ProxyValueCallback implements com.tencent.smtt.sdk.ValueCallback<String> {

    private android.webkit.ValueCallback<String> valueCallback;

    public X5ProxyValueCallback(android.webkit.ValueCallback<String> callback) {
        valueCallback = callback;
    }

    @Override
    public void onReceiveValue(String s) {
        if (valueCallback != null) {
            valueCallback.onReceiveValue(s);
        }
    }
}
