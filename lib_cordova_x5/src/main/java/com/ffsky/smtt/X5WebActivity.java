package com.ffsky.smtt;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;


public class X5WebActivity extends AppCompatX5WebActivity {

    private static final String KEY_URL = "__URL";

    public static void start(Activity activity, String url) {
        Intent i = new Intent(activity, X5WebActivity.class);
        if (!TextUtils.isEmpty(url)) {
            i.putExtra(KEY_URL, url);
        }
        activity.startActivity(i);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        if (i.hasExtra(KEY_URL)) {
            launchUrl = i.getStringExtra(KEY_URL);
        }
        loadUrl(launchUrl);
    }
}
