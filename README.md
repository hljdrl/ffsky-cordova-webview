
# Cordova WebView aar组件


#### aar快捷引用 
1.   api 'com.gitee.hljdrl:cordova-framework:9.1.0003@aar'   
2.   api 'com.gitee.hljdrl:cordova-plugins:9.1.0003@aar'     
3.   api 'com.gitee.hljdrl:cordova-appx:9.1.0003@aar'
4.   api 'com.gitee.hljdrl:cordova-x5:9.1.0003@aar'


#### 使用说明-项目引用库
1.  cordova webview核心库          api 'com.gitee.hljdrl:cordova-framework:9.1.0@aar'
2.  cordova plugin-android插件库   api 'com.gitee.hljdrl:cordova-plugins:9.1.0@aar'
3.  最新版本搜索： https://search.maven.org/search?q=com.gitee.hljdrl


#### 版本特性 9.1.0003
1.   增加com.gitee.hljdrl-cordova-app-x扩展组件，
     组件: AppCompatWebActivity、BaseTabWebActivity、WebActivity
2.   增加x5 webview接入
     组件：AppCompatX5WebActivity、X5WebActivity、X5ModularLoader
     

####  需要优化
1.  实际接入到项目中,需要针对插件优化，线程池优化,X5WebView接入 ，Cordova WebView的设计模式未单页面webview,
    然后很多项目中要求多页面WebView, 

<img src="art/webview.gif" />


#### cordova plugin插件库-列表【插件配置文件需要放到app模块-src->main->res->xml目录下】
1.   Battery        org.apache.cordova.batterystatus.BatteryListener
2.   Camera         org.apache.cordova.camera.CameraLauncher
3.   Device         org.apache.cordova.device.Device
4.   Notification   org.apache.cordova.dialogs.Notification
5.   File           org.apache.cordova.file.FileUtils
6.   InAppBrowser   org.apache.cordova.inappbrowser.InAppBrowser
7.   Media          org.apache.cordova.media.AudioHandler
8.   Capture        org.apache.cordova.mediacapture.Capture
9.   NetworkStatus  org.apache.cordova.networkinformation.NetworkManager
10.  CDVOrientation cordova.plugins.screenorientation.CDVOrientation
11.  StatusBar      org.apache.cordova.statusbar.StatusBar
12.  Whitelist      org.apache.cordova.whitelist.WhitelistPlugin


#### config.xml文件内容

```
<?xml version='1.0' encoding='utf-8'?>
<widget id="io.cordova.hellocordova" version="1.0.0" xmlns="http://www.w3.org/ns/widgets" xmlns:cdv="http://cordova.apache.org/ns/1.0" >
    <feature name="Battery">
        <param name="android-package" value="org.apache.cordova.batterystatus.BatteryListener" />
    </feature>
    <feature name="Camera">
        <param name="android-package" value="org.apache.cordova.camera.CameraLauncher" />
    </feature>
    <feature name="Device">
        <param name="android-package" value="org.apache.cordova.device.Device" />
    </feature>
    <feature name="Notification">
        <param name="android-package" value="org.apache.cordova.dialogs.Notification" />
    </feature>
    <feature name="File">
        <param name="android-package" value="org.apache.cordova.file.FileUtils" />
        <param name="onload" value="true" />
    </feature>
    <allow-navigation href="cdvfile:*" />
    <feature name="InAppBrowser">
        <param name="android-package" value="org.apache.cordova.inappbrowser.InAppBrowser" />
    </feature>
    <feature name="Media">
        <param name="android-package" value="org.apache.cordova.media.AudioHandler" />
    </feature>
    <feature name="Capture">
        <param name="android-package" value="org.apache.cordova.mediacapture.Capture" />
    </feature>
    <feature name="NetworkStatus">
        <param name="android-package" value="org.apache.cordova.networkinformation.NetworkManager" />
    </feature>
    <feature name="CDVOrientation">
        <param name="android-package" value="cordova.plugins.screenorientation.CDVOrientation" />
    </feature>
    <feature name="StatusBar">
        <param name="android-package" value="org.apache.cordova.statusbar.StatusBar" />
        <param name="onload" value="true" />
    </feature>
    <feature name="Whitelist">
        <param name="android-package" value="org.apache.cordova.whitelist.WhitelistPlugin" />
        <param name="onload" value="true" />
    </feature>
    <name>HelloCordova</name>
    <description>
        A sample Apache Cordova application that responds to the deviceready event.
    </description>
    <author email="dev@cordova.apache.org" href="http://cordova.io">
        Apache Cordova Team
    </author>
    <content src="index.html" />
    <access origin="*" />
    <allow-intent href="http://*/*" />
    <allow-intent href="https://*/*" />
    <allow-intent href="tel:*" />
    <allow-intent href="sms:*" />
    <allow-intent href="mailto:*" />
    <allow-intent href="geo:*" />
    <allow-intent href="market:*" />
    <preference name="loglevel" value="DEBUG" />
</widget>

```


#### 关乎aar版本号
1.   版本号采用cordova版本后 + xxx ,例如 9.1.0001 ，9.1.0为cordova官方版本号，001为aar打包版本号.


源码来自 https://cordova.apache.org/ 版本 9.1.0 使用 npm cordova cli构建
